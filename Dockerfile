FROM python:3.7-slim

RUN apt-get update && \
    apt-get install -yq make gcc && \
    rm -rf /var/lib/apt/lists/* && \
    pip install sanic==0.8.3 && \
    apt-get autoremove -y gcc make

